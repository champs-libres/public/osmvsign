from qgis.core import QgsProject, QgsVectorLayer, QgsExpression, QgsVectorFileWriter

from qgis.utils import iface
import processing

mc = iface.mapCanvas()
pjt = QgsProject.instance()

# OPTION 1: Add layer from an empty project
# QgsProject.instance().removeAllMapLayers()

# appel des deux couches initiales
# ign = QgsVectorLayer("C:/Users/LENOVO/Documents/Cours/Master_1/stage/OSM-IGN/Wavre/ign/ign_wavre.shp", "ign", "ogr")
# osm = QgsVectorLayer("C:/Users/LENOVO/Documents/Cours/Master_1/stage/OSM-IGN/Wavre/osm/osm_wavre_3812.shp", "osm",
#                      "ogr")

# QgsProject.instance().addMapLayer(osm)
# QgsProject.instance().addMapLayer(ign)

# OPTION 2: Get layers from a given project
ign = pjt.mapLayersByName('IGN')[0]
osm = pjt.mapLayersByName('OSM')[0]

# Traitement de la carte IGN ------------------------------------

# Sélection des routes qui nous intéressent
result1 = processing.run("native:extractbyattribute",
                         {'INPUT': ign, 'FIELD': "roadstatus", 'OPERATOR': 1, 'VALUE': 7, 'OUTPUT': 'memory:'})
ign_filtr_layer = result1['OUTPUT']
QgsProject.instance().addMapLayer(ign_filtr_layer)
ign_filtr_layer.setName('ign_filtr')

# Calcul des longueurs (m) des segments de route
expre1 = "$length"
result2 = processing.run("native:fieldcalculator",
                         parameters={'INPUT': ign_filtr_layer, 'FIELD_NAME': 'length_m', 'FIELD_TYPE': 0, \
                                     'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False, 'FORMULA': expre1,
                                     'OUTPUT': 'memory:'})
ign_filtr_len_layer = result2['OUTPUT']
QgsProject.instance().addMapLayer(ign_filtr_len_layer)
ign_filtr_len_layer.setName('ign_filtr_len')

# Traitement de la carte OSM ------------------------------------
# Calcul des longueurs des segments de route
result3 = processing.run("native:fieldcalculator",
                         parameters={'INPUT': osm, 'FIELD_NAME': 'length_m', 'FIELD_TYPE': 0, \
                                     'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False, 'FORMULA': expre1,
                                     'OUTPUT': 'memory:'})
osm_len_layer = result3['OUTPUT']
QgsProject.instance().addMapLayer(osm_len_layer)
osm_len_layer.setName('osm_len')

# Sélection des routes qui nous intéressent
expre2 = "highway = 'residential' OR highway = 'primary' OR highway = 'secondary' OR highway = 'tertiary'\
OR highway = 'service' OR highway = 'living-street' OR highway = 'unclassified' OR highway = 'trunk'\
OR highway = 'motorway' OR highway = 'motorway_link' OR highway = 'trunk_link'\
OR highway = 'primary_link' OR highway = 'secondary_link' OR highway = 'tertiary_link'"
result4 = processing.run("qgis:selectbyexpression",
                         {'INPUT': osm_len_layer, 'EXPRESSION': expre2, 'OUTPUT': 'memory:'})

result5 = processing.run("native:saveselectedfeatures",
                         {'INPUT': osm_len_layer, 'OUTPUT': 'memory:'})
osm_filtr_layer = result5['OUTPUT']
QgsProject.instance().addMapLayer(osm_filtr_layer)
osm_filtr_layer.setName('osm_filtr')

# Création d'un buffer
result6 = processing.run("native:buffer",
                         {'INPUT': osm_filtr_layer, 'DISTANCE': 3, 'SEGMENTS': 4, 'END_CAP_STYLE': 2, 'JOIN_STYLE': 0,
                          'MITER_LIMIT': 2,
                          'DISSOLVE': False, 'OUTPUT': 'memory:'})
osm_filtr_buff_layer = result6['OUTPUT']
QgsProject.instance().addMapLayer(osm_filtr_buff_layer)
osm_filtr_buff_layer.setName('osm_filtr_buff')

# Découpage de la carte OSM aux limites de la carte IGN
# Création d'une couche d'emprise autour d'IGN
result7 = processing.run("native:polygonfromlayerextent",
                         {'INPUT': ign, 'OUTPUT': 'memory:'})
emprise_layer = result7['OUTPUT']
QgsProject.instance().addMapLayer(emprise_layer)
emprise_layer.setName('emprise')

# Découpage
result8 = processing.run("native:clip",
                         {'INPUT': osm_filtr_buff_layer, 'OVERLAY': emprise_layer,
                          'OUTPUT': 'memory:'})
osm_filtr_buff_clip_layer = result8['OUTPUT']
QgsProject.instance().addMapLayer(osm_filtr_buff_clip_layer)
osm_filtr_buff_clip_layer.setName('osm_filtr_buff_clip')

# Création de la colonne contenant le nombre de voies pour la couche OSM

expre3 = "if(lanes is NULL,\
CASE WHEN highway IN ('motorway' , 'primary' , 'secondary' , 'trunk') THEN 2\
WHEN highway IN ('residential' , 'tertiary' , 'service' , 'living_street' , 'unclassified') THEN 1\
END, lanes)"

result9 = processing.run("native:fieldcalculator",
                         parameters={'INPUT': osm_filtr_buff_clip_layer, 'FIELD_NAME': 'lanes_osm', 'FIELD_TYPE': 1, \
                                     'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False, 'FORMULA': expre3,
                                     'OUTPUT': 'memory:'})
osm_filtr_buff_clip_lane_layer = result9['OUTPUT']
QgsProject.instance().addMapLayer(osm_filtr_buff_clip_lane_layer)
osm_filtr_buff_clip_lane_layer.setName('osm_filtr_buff_clip_lane')

# Boucle repeat pour améliorer la précision
x = 1
while x < 10:
    # rename la dernière couche en sortie de la boucle pour qu'elle ait le même nom que celle d'entrée de la boucle
    osm_repeat_layer = iface.activeLayer()
    # Création d'une colonne RNG
    expre4 = "rand(1,2)"
    result10 = processing.run("native:fieldcalculator",
                              parameters={'INPUT': osm_repeat_layer, 'FIELD_NAME': 'rng', 'FIELD_TYPE': 1, \
                                          'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False,\
                                          'FORMULA': expre4,\
                                          'OUTPUT': 'memory:'})
    osm_rng_layer = result10['OUTPUT']
    QgsProject.instance().addMapLayer(osm_rng_layer)
    osm_rng_layer.setName('osm_rng')

    result11 = processing.run("native:extractbyattribute",
                              {'INPUT': osm_rng_layer, 'FIELD': "rng", 'OPERATOR': 0, 'VALUE': 1, 'OUTPUT': 'memory:'})
    osm_rng1_layer = result11['OUTPUT']
    QgsProject.instance().addMapLayer(osm_rng1_layer)
    osm_rng1_layer.setName('osm_rng1')

    result12 = processing.run("native:extractbyattribute",
                              {'INPUT': osm_rng_layer, 'FIELD': "rng", 'OPERATOR': 0, 'VALUE': 2, 'OUTPUT': 'memory:'})
    osm_rng2_layer = result12['OUTPUT']
    QgsProject.instance().addMapLayer(osm_rng2_layer)
    osm_rng2_layer.setName('osm_rng2')

    result13 = processing.run("native:symmetricaldifference",
                              {'INPUT': osm_rng1_layer, 'OVERLAY': osm_rng2_layer, 'OUTPUT': 'memory:'})
    sym_dif_layer = result13['OUTPUT']
    QgsProject.instance().addMapLayer(sym_dif_layer)
    sym_dif_layer.setName('sym_dif')

    # regrouper les colonnes
    expre5 = "case\
    when length_m is null\
    then length_m_2\
    else length_m\
    end\
    "
    result14 = processing.run("native:fieldcalculator",
                              parameters={'INPUT': sym_dif_layer, 'FIELD_NAME': 'length_m', 'FIELD_TYPE': 0, \
                                          'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False,
                                          'FORMULA': expre5, 'OUTPUT': 'memory:'})
    clean_transi_sym_dif_layer = result14['OUTPUT']
    QgsProject.instance().addMapLayer(clean_transi_sym_dif_layer)
    clean_transi_sym_dif_layer.setName('clean_transi_sym_dif')

    expre6 = "case\
    when lanes_osm is null\
    then lanes_osm_2\
    else lanes_osm\
    end\
    "
    result15 = processing.run("native:fieldcalculator",
                              parameters={'INPUT': clean_transi_sym_dif_layer, 'FIELD_NAME': 'lanes_osm',\
                                          'FIELD_TYPE': 1, \
                                          'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': False,\
                                          'FORMULA': expre6, 'OUTPUT': 'memory:'})
    clean_sym_dif_layer = result15['OUTPUT']
    QgsProject.instance().addMapLayer(clean_sym_dif_layer)
    clean_sym_dif_layer.setName('clean_sym_dif')

    #Nettoyage des couches
    QgsProject.instance().removeMapLayer(osm_rng_layer)
    QgsProject.instance().removeMapLayer(osm_rng1_layer)
    QgsProject.instance().removeMapLayer(osm_rng2_layer)
    QgsProject.instance().removeMapLayer(clean_transi_sym_dif_layer)
    QgsProject.instance().removeMapLayer(sym_dif_layer)

    #Nettoyage des colonnes superflues
    layer = iface.activeLayer()
    layer.startEditing()
    dp = layer.dataProvider()
    field_ids = []
    fieldnames = ['full_id', 'lanes_osm', 'length_m',]
    for field in dp.fields():
        if field.name() not in fieldnames:
            field_ids.append(dp.fieldNameIndex(field.name()))
    dp.deleteAttributes(field_ids)
    layer.updateFields() #p-e pas nécessaire, double usage avec commitchanges
    layer.commitChanges()

    # addition de 1 à x pour pas que la boucle tourne à l'infini
    x += 1


sortie_last_itte = iface.activeLayer()

#Jointure par localisation des couches OSM et IGN traitées
result16 = processing.run("native:joinattributesbylocation",
                          {'INPUT':sortie_last_itte, 'JOIN':ign_filtr_len_layer, 'PREDICATE':6,\
                           'SUMMARY':0,'KEEP':1,'OUTPUT':'memory:'})
join_layer = result16['OUTPUT']
QgsProject.instance().addMapLayer(join_layer)
join_layer.setName('join')

#Extraction des données qui nous intéressent

expre7 = ['lanes_osm','lanesnb']
result17 = processing.run("qgis:statisticsbycategories",
                          {'INPUT':join_layer, 'VALUES_FIELD_NAME':'length_m','CATEGORIES_FIELD_NAME': expre7, 'OUTPUT':'C:/Users/LENOVO/Documents/Cours/Master_1/stage/OSM-IGN/Sortie.shp'})
