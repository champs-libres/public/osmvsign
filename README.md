OSM vs IGN
-------------------------

# Purpose

This code aims to quantify the differences between OSM and IGN maps. The whole point is that OSMs are free but IGN are rather expensive,

so the idea was to compare the both of them and to see if the OSMs could be used instead of the IGN maps.


# How?

## Preparation of data

Get some IGN and OSM data and store it respectively in the `data/ign` and `data/osm` folder. Then, this data are called in the `code_final.py` file (you may adapt the path to get the data).

For OSM data, one conveninet way is to use the QuickOSM QGIS plugin and to query the `highway` line data as follows:

- query the data with the layer extent of the IGN layer
- select only "way" and output only "lines"

![](doc/img/quickosm.png)

## Run the analysis

This is done by the code named 'code_final.py' which basically gives QGIS a series of instructions.

This pyqgis code tries to express those differences in the most objective way.

Many steps were created between the input maps and the final product.

The highways were filtered, the number of lanes in the OSMs were calculated etc.


# Output

The result of this script is a table that compares the number of lanes registered on the roads of the two maps.

The number of lanes is compared in terms of kilometers and of segments.


# Input

You will have to have two maps to compare and call then at lines 10 and 11 of the code.

The other thing you will probably have to modify is the path of the table that the code produces (line 202)

You can easily get any OSM map you are interested in by using the QuickOSM plugin on QGIS.


Champs-Libres scrlfs 2021
info@champs-libres.coop

